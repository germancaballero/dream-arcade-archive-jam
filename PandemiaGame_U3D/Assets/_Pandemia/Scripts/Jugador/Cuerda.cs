﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EstadoCuerda
{
    Esperando,
    SoltandoCuerda,
    Colgando
}

public class Cuerda : MonoBehaviour
{
    public MovimientoJug movJug;
    public  LineRenderer line;
    public Transform centroGiro;
    public Sprite colgando;
    private float velIniY;
    private float velMinY;
    private EstadoCuerda estado;
    private Vector2 centroGiroIni;
    private Vector2 difCuerdaYJug;
    private float timeIni;
    private float radio;
    private float anguloIni;
    

    private Vector3 rotacionIni;
    private GameObject esfera;

    // Start is called before the first frame update
    void OnEnable()
    {
      /*  Debug.Log("" + Vector2.SignedAngle(-Vector2.one * 10, Vector2.one * 10) * 1000);
        Debug.Log("" + Vector2.SignedAngle(Vector2.left * 10, Vector2.right * 10) * 1000);
        Debug.Log("" + Vector2.SignedAngle(Vector2.zero, Vector2.up * 10) * 1000);
        Debug.Log("" + Vector2.SignedAngle(Vector2.zero, Vector2.left * 10) * 1000);*/

        velMinY = velIniY = movJug.velMov.y;
        estado = EstadoCuerda.Esperando;
        //velMinY = velIniY;
        line.enabled = false;
        rotacionIni = transform.localEulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        if (estado != EstadoCuerda.SoltandoCuerda)
        {
            if (movJug.velocidad.y >= velMinY)
            {
                velMinY = movJug.velMov.y;
                estado = EstadoCuerda.Esperando;
            }
            else if (movJug.velocidad.y <= velMinY )
            {
                ComenzarGiroCuerda();
            }
        } else
        {
            var tiempo = Time.time - timeIni;
            float angulo;
            if (movJug.velocidad.x > 0)   // Colgando y girando hacia la derecha
            {
                angulo = anguloIni + Mathf.Pow(tiempo, 2) * 1.5f;
            }
            else if (movJug.velocidad.x < 0)   // Colgando hacia la izquierda
            {
                angulo = anguloIni - Mathf.Pow(tiempo, 2) * 1.5f;
            } else
            {
                angulo = anguloIni;
            }
            movJug.transform.position = centroGiroIni + difCuerdaYJug + new Vector2(
                Mathf.Cos(angulo), Mathf.Sin(angulo)) * radio;
            this.transform.localEulerAngles = new Vector3(0, 0, (angulo + Mathf.PI) * Mathf.Rad2Deg);
            
            if (Mathf.Abs( angulo - anguloIni) > Mathf.PI * 2 / 3f
                || tiempo > 1.5f)
            {
                SoltarCuerda();
            }
        }

    }
    void ComenzarGiroCuerda()
    {
        estado = EstadoCuerda.SoltandoCuerda;
        movJug.enabled = false;
        line.enabled = true;
        timeIni = Time.time;
        movJug.anim.speed = 0;
        /*line.transform.localScale = new Vector3(
            line.transform.localScale.x, line.transform.localScale.y, 0.3f);*/
        centroGiroIni = centroGiro.position;
        esfera = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        esfera.transform.position = centroGiro.position;
        esfera.transform.localScale = Vector3.one / 10f;
        difCuerdaYJug = movJug.transform.position - transform.position;
        radio = Vector2.Distance(transform.position, centroGiroIni);
        /*anguloIni = Mathf.Atan2(
              movJug.transform.position.x - centroGiroIni.x,
              movJug.transform.position.y - centroGiroIni.y);*/
        anguloIni = Vector2.SignedAngle(Vector2.right, (Vector2)transform.position - centroGiroIni)
            * Mathf.Deg2Rad;// -Mathf.PI * 0.7f;
        Debug.Log("Radio: " + radio
            + "\nanguloIni: " + anguloIni
            + "\nVector: " + (centroGiroIni - (Vector2)transform.position)
            + "\nmovJug.transform.position: " + transform.position
            + "\ncentroGiroIni: " + centroGiroIni);
        movJug.spr.sprite = colgando;
    }
    void SoltarCuerda()
    {
        this.gameObject.SetActive(false);
        movJug.enabled = true;
        transform.localEulerAngles = rotacionIni;
        //Destroy(esfera.gameObject);
    }
}
